/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 *  Configuration header for the BMG_160 module.
 *
 * ****************************************************************************/

/* header definition ******************************************************** */

#ifndef BMG_160_CH_H_
#define BMG_160_CH_H_

/* local interface declaration ********************************************** */

#include "FreeRTOS.h"
#include "timers.h"

/* local type and macro definitions */
#define PGD_THREESECONDDELAY                UINT32_C(3000)     /**< three second is represented by this macro */
#define PGD_TIMERBLOCKTIME                  UINT32_C(0xffff)   /**< Macro used to define blocktime of a timer */
#define PGD_ZERO                            UINT8_C(0)         /**< default value */
#define PGD_ONE                             UINT8_C(1)         /**< default value */

/* local function prototype declarations */
/**
 * @brief Read data from Gyro sensor and print through the USB
 *
 * @param[in] pxTimer timer handle
 */
void bmg160_getSensorValues(xTimerHandle pxTimer);

/* local module global variable declarations */

/* local inline function definitions */
#endif /* BMG_160_CH_H_ */

/** ************************************************************************* */
