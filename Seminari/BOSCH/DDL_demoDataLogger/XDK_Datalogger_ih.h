/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file
 *
 *  Interface header for the XDK_Datalogger Module.
 *
 * The interface header exports the following features:  PB0_Init,
 *                                                       Sensor_init,
 * 														 PSD_deinit,
 *														 SDC_writeBackBuffer,
 *														 sampleSensors,
 *														 stringReplace,	
 *														 writeSensorDataCsvHeader,
 *														 writeSensorDataCsv,
 *														 writeSensorDataJson,
 *														 writeSensorDataJsonHeader,
 *														 writeSensorDataJsonFooter,
 *														 writeSensorDataCustom,
 *														 writeLogEntry,
 *														 writeLogHeader,
 *														 writeLogFooter
 *
 * ****************************************************************************/
/**
 * @defgroup psd PSD
 * @ingroup APP
 *
 * @{
 * @brief  Printing available sensor Data using USB printf periodically,triggered by a timer(free Rtos)
 *  \tableofcontents
 *  \section intro_sec PSD
 * Application to log all the defined sensors on SD-Card
 * every one ms, initiated by auto reloaded Tasks(freertos)
 */
/* header definition ******************************************************** */
#ifndef XDK_DATALOGGER_IH_H_
#define XDK_DATALOGGER_IH_H_

#include <string.h>
#include "XDK_Datalogger_ch.h"
/* public interface declaration ********************************************* */
#define BUFFSIZE  19500


 /* Priorities */
#define TASK_PRIO_MAIN_CMD_PROCESSOR                (UINT32_C(1))
#define TASK_STACK_SIZE_MAIN_CMD_PROCESSOR          (UINT16_C(700))
#define TASK_Q_LEN_MAIN_CMD_PROCESSOR                (UINT32_C(10))

/* local function prototype declarations */
void appInitSystem(void * CmdProcessorHandle, uint32_t param2);

/* public type and macro definitions */
typedef enum SDC_sdCardAppReturn_e
{
    SDC_APP_ERR_DEINIT_FAILED = INT8_C(-1), /**< SD-Card DeInitialize failure */
    SDC_APP_ERR_INIT_FAILED = INT8_C(-2), /**< SD-Card Initialize failure */
    SDC_APP_ERR_ERROR = INT8_C(-3), /**< SD-Card Non-Error return */
    SDC_APP_ERR_WRITE_FAILURE = INT8_C(-4), /**< SD Card return write failure */
    SDC_APP_ERR_READ_FAILURE = INT8_C(-5), /**< SD Card return read failure */
    SDC_APP_ERR_NOTREADY_FAILURE = INT8_C(-6), /**< SD Card return Not ready failure */
    SDC_APP_ERR_NO_ERROR = INT8_C(0) /**< SD Card Error Return */
} SDC_sdCardAppReturn_t;

typedef struct
{
    int32_t length;
    char data[BUFFSIZE];
} Buffer;

/* public function prototype declarations */

/**
 * @brief Initialize Data Logger software module
 */
extern void dl_init(void);
/**
 * @brief
 *      Init-function for Button1
 */
void PB0_Init(void);

/* public global variable declarations */

/* inline function definitions */

/**
 * @brief The function initializes Data logger which calls the individual sensor update functions
 * The functions updates raw and unit data of the sensors. Only sensors enabled in logger.ini will be initialized
 * Additional the Function downgrades the fastest sampling rate if its necessary to ensure a constant logging rate
 */
extern void Sensor_init(void);

/**
 *  @brief API to de-initialize the PSD module
 */
extern void PSD_deinit(void);

/* public global variable declarations */

/* inline function definitions */

/**
 * @brief
 *      Task to write the Data stored in Back buffer to SD-Card, and handle the create process for a new file.
 *
 *  @param[in] void *pvParameters
 */
void SDC_writeBackBuffer(void *pvParameters);

/**
 * \brief function for getting the sensor data, by calling the Sensor-API
 *  getting the data for all sensors and handling the individual sensor sample rates
 *
 *
 * @param   conf - pointer to configuration struct
 */
uint8_t sampleSensors(configuration *conf);

/**
 * \brief function string replace.
 *  search a substring in a string and replace it with a different string
 *
 * @param[in]   search - string to search
 * @param[in]   replace -string to replace with
 * @param[in]   string -string where replacement is made in
 *
 * @return  result string after replacement
 */
char * stringReplace(char *search, char *replace, char *string);

/**
 * \brief function for writing csv format header to the ActiveBuffer
 *
 *
 * @param[in]   conf - pointer to configuration struct
 */
void writeSensorDataCsvHeader(configuration *conf);

/**
 * \brief function for writing csv format sensor data to the ActiveBuffer
 *
 *
 * @param[in]   timestamp= timestamp in ms since start of logging, conf=pointer to configuration struct,
 */
void writeSensorDataCsv(uint64_t timestamp, configuration *conf);

/**
 * \brief function for writing json format sensor data to the ActiveBuffer
 *
 *
 * @param[in]   timestamp - time stamp in ms since start of logging, serial number=serial number, conf=pointer to configuration struct
 */
void writeSensorDataJson(uint64_t timestamp, uint64_t serialnumber,
        configuration *conf);

/**
 * \brief function for writing json format header to the ActiveBuffer
 *
 *
 * @param[in]    conf=pointer to configuration struct
 */
void writeSensorDataJsonHeader(configuration *conf);

/**
 * \brief function for writing json format footer
 *
 * @param[in]    buffer, the ActiveBuffer
 */
void writeSensorDataJsonFooter(FIL *fileObject);

/**
 * \brief function for writing custom format sensor data
 *
 *
 * @param[in]   customstring=user defined format string, time stamp= timestamp in ms since start of logging, conf=pointer to configuration struct,
 */
void writeSensorDataCustom(char *customstring, uint64_t timestamp,
        configuration *conf);

/**
 * \brief function for writing sensor data into the log file
 * depending on configuration raw or unit data and csv, json or custom file format
 *
 * @param[in]   customstring=user defined format string,  conf=pointer to configuration struct
 */
void writeLogEntry(configuration *conf, char *customstring, uint32_t timestamp,
        uint32_t serialnumber);

/**
 * \brief function for writing header into the log file
 * depending on configuration csv, json or custom file format
 *
 * @param[in]   customheader=user defined header,  conf=pointer to configuration struct
 */
void writeLogHeader(configuration *conf, char *customheader);

/**
 * \brief function for writing footer into the log file
 * depending on configuration. Footer is written for json file format only
 *
 * @param[in]   conf=pointer to configuration struct
 */
void writeLogFooter(configuration *conf, FIL *fileObject);

/* local interface declaration ********************************************** */
 /* Priorities */
#define TASK_PRIO_MAIN_CMD_PROCESSOR                (UINT32_C(1))
#define TASK_STACK_SIZE_MAIN_CMD_PROCESSOR          (UINT16_C(700))
#define TASK_Q_LEN_MAIN_CMD_PROCESSOR                (UINT32_C(10))
/* local type and macro definitions */
#define ZERO    UINT8_C(0)  			            /**< Macro to define value zero*/

//#warning Please provide WLAN related configurations, with valid SSID & WPA key and server ip address where packets are to be sent in the below macros.
/** Network configurations */
#define WLAN_CONNECT_WPA_SSID   "iPhone Bosch di Andrea D."		/**< Macros to define WPA/WPA2 network settings */
#define WLAN_CONNECT_WPA_PASS	"ci986s7gsv871"					/**< Macros to define WPA/WPA2 network settings */
#define SERVER_PORT				10319

#include "timers.h"

#define MILLISECONDS(x) ((portTickType) x / portTICK_RATE_MS)
#define SECONDS(x) ((portTickType) (x * 1000) / portTICK_RATE_MS)

/**
 * Represents the Operation State of the device.
 */
typedef uint32_t OperationState;

/**
 * PTP Message Types.
 */
enum PtpMessageTypes
{
	SYNC = (uint8_t) 1,
	DELAY_REQ,
	DELAY_RESP,
	DELAY_ACK
};

/**
 * Represents the message type of a PTP type.
 */
typedef uint8_t PtpMessageType;

/**
 * Represents the PTP Packet
 */
typedef struct ptp_packet_s{
	PtpMessageType 	type;
	uint8_t			validFields;
	TickType_t		syncSendTime;
	TickType_t		syncRecvTime;
	TickType_t		dreqSendTime;
	TickType_t		dreqRecvTime;
	TickType_t		dresSendTime;
	TickType_t		dresRecvTime;
} PtpPacket_t;

/* local function prototype declarations */
void transitionTo(OperationState);

enum XDK_App_Retcode_E
{
    SEMAPHORE_TIME_OUT_ERROR= RETCODE_FIRST_CUSTOM_CODE,
    SEMAPHORE_CREATE_ERROR,
    SEMAPHORE_GIVE_ERROR,
};

void dl_appInitSystem(void);

#endif /* XDK_DATALOGGER_IH_H_ */
/** ************************************************************************* */
