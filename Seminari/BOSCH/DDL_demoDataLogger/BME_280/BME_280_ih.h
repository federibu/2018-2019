/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 *  Interface header for the BME_280 module.
 *
 * The interface header exports the following features:  bme_280_init,
 *                                                       bme_280_deInit
 * ****************************************************************************/
/**
 * @defgroup ped PED
 * @ingroup APP
 *
 * @{
 * @brief  Printing Environmental Data using USB printf periodically,triggered by a timer(free Rtos)
 *  \tableofcontents
 *  \section intro_sec PED
 * Demo application of printing BME280 Environmental data on serial port(USB virtual com port)
 * every one second, initiated by auto reloaded timer(freeRTOS)
 */
/* header definition ******************************************************** */
#ifndef BME_280_IH_H_
#define BME_280_IH_H_

#include "BCDS_Environmental.h"
/* public interface declaration ********************************************* */

/* public global variable declarations */
extern Environmental_Data_T bme280s;
extern Environmental_LsbData_T bme280lsb;

/* public type and macro definitions */

/* public function prototype declarations */

/**
 * @brief The function initializes BME(Environmental)creates and starts a autoreloaded
 * timer task which gets and prints the Environmental raw data and actual data
 *
 */
extern void bme_280_init(void);

/**
 *  @brief API to deinitialize the PED module
 */
extern void bme_280_deInit(void);

/* inline function definitions */

/**@}*/
#endif /* BME_280_IH_H_ */

/** ************************************************************************* */
