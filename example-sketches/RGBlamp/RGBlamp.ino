/**
 * - collegare un potenziometro ad un piedino analogico
 * - collegare un led rgb a tre piedini digitali PWM (!)
 *
 * fare in modo che ruotando il potenziometro il colore del led cambi secondo l'arcobaleno
 * cfr. https://stackoverflow.com/questions/3407942/rgb-values-of-visible-spectrum
 *
 * in realtà ci sono già esempi fatti nelle lib ESP32 per ArduinoIDE:
 * + Examples->ESP32->AnalogOut->ledcWrite_RGB per modulare un LED RGB
 * + Examples->03.Analog->AnalogInput per leggere un potenziometro
 * basta "mescolare" i due esempi...
 *
 */

// fa schifo dover usare i define lo so...
//#define ESP32  // questo define è già incluso nella def della board?
//#define USOESP32
//#define DEBUGRGB

// TODO pesare i vari colori (https://www.hackster.io/techmirtz/using-common-cathode-and-common-anode-rgb-led-with-arduino-7f3aa9)
// N.B. attenzione se led è common-cathode (massa comune) o common-anode (positivo comune)

// in realtà invertite perché LED è a positivo comune
#define MINVAL 500
#define MAXVAL 1023
//#define IDIOTA

#ifdef USOESP32
const int sensorPin = ...;    // select the input pin for the potentiometer
const int ledRpin = ...;
const int ledGpin = ...;
const int ledBpin = ...;
#else
const int sensorPin = A0;    // select the input pin for the potentiometer
const int ledRpin = D1;
const int ledGpin = D2;
const int ledBpin = D3;
#endif

// considerazioni memoria?
int potenziometro = 0;  // variable to store the value coming from the sensor
int vR,vG,vB;

void setup() {
    // serial
    Serial.begin(115200);

    // tanto per far lampeggiare qualcosa
    pinMode(LED_BUILTIN, OUTPUT);

    // pinmodes
#ifdef USOESP32
    ...
#else

#endif
}


void loop() {
    digitalWrite(LED_BUILTIN, HIGH);

    // legge potenziometro
#ifdef USOESP32
    ...
#else
    potenziometro = analogRead(sensorPin);
#endif

    //Serial.println(potenziometro);
    potenziometro=map(potenziometro,0,1023,400,700); // per portare il range a quello dei nm

    // calcola nuova tripla RGB
    //calcRGB();
    //impostaRGB();

    spectral_color();
    printRGB();

    // aggiusta colore
    adjustRGB();

    // se non metto delay gira alla massima velocità possibile
    delay(20);

    digitalWrite(LED_BUILTIN, LOW);
}

/** aggiusta i valori RGB del led
 */
void adjustRGB() {
#ifdef USOESP32
    ...
#else
    analogWrite(ledRpin, MAXVAL-vR);
    analogWrite(ledGpin, MAXVAL-vG);
    analogWrite(ledBpin, MAXVAL-vB);
#endif
}

/** print tre valori RGB (per serial plotter)
 */
void printRGB() {
    //Serial.print(potenziometro);
    //Serial.print(",");
    Serial.print(vR);
    Serial.print(",");
    Serial.print(vG);
    Serial.print(",");
    Serial.println(vB);
}

/** a partire da un intero (letto da potenziometro) calcola tripla RGB (cfr. StackOverflow citato in cima)
 */
void calcRGB() {
    // versione "idiota"
#ifdef IDIOTA
    vR=random(MINVAL,MAXVAL);
    vG=random(MINVAL,MAXVAL);
    vB=random(MINVAL,MAXVAL);
#else
    vR=MAXVAL;
    vG=MAXVAL;
    vB=MAXVAL;
#endif
}

/** solo per avere un modo di settare a valori deterministici per testing
 */
void impostaRGB() {
    vR++;
    vG++;
    vB++;
    if(vR>MAXVAL) {
        vR=MINVAL;
        vG=MINVAL;
        vB=MINVAL;
    }
}

// presa dal post su StackOverflow e adattata
void spectral_color() { // RGB <0,1> <- lambda l <400,700> [nm]
    double l=potenziometro; // serve conversione?
    double t,r,g,b;
    if ((l>=400.0)&&(l<410.0)) {
        t=(l-400.0)/(410.0-400.0);
        r=    +(0.33*t)-(0.20*t*t);
    } else if ((l>=410.0)&&(l<475.0)) {
        t=(l-410.0)/(475.0-410.0);
        r=0.14         -(0.13*t*t);
    } else if ((l>=545.0)&&(l<595.0)) {
        t=(l-545.0)/(595.0-545.0);
        r=    +(1.98*t)-(     t*t);
    } else if ((l>=595.0)&&(l<650.0)) {
        t=(l-595.0)/(650.0-595.0);
        r=0.98+(0.06*t)-(0.40*t*t);
    } else if ((l>=650.0)&&(l<700.0)) {
        t=(l-650.0)/(700.0-650.0);
        r=0.65-(0.84*t)+(0.20*t*t);
    }
    if ((l>=415.0)&&(l<475.0)) {
        t=(l-415.0)/(475.0-415.0);
        g=             +(0.80*t*t);
    } else if ((l>=475.0)&&(l<590.0)) {
        t=(l-475.0)/(590.0-475.0);
        g=0.8 +(0.76*t)-(0.80*t*t);
    } else if ((l>=585.0)&&(l<639.0)) {
        t=(l-585.0)/(639.0-585.0);
        g=0.84-(0.84*t)           ;
    }
    if ((l>=400.0)&&(l<475.0)) {
        t=(l-400.0)/(475.0-400.0);
        b=    +(2.20*t)-(1.50*t*t);
    } else if ((l>=475.0)&&(l<560.0)) {
        t=(l-475.0)/(560.0-475.0);
        b=0.7 -(     t)+(0.30*t*t);
    }

#ifdef DEBUGRGB
    Serial.print(r);
    Serial.print(",");
    Serial.print(g);
    Serial.print(",");
    Serial.println(b);
#endif

    vR=MAXVAL*r;
    vG=MAXVAL*g;
    vB=MAXVAL*b;
}

// idem
void spectral_color2() { // RGB <0,1> <- lambda l <400,700> [nm]
    double w=potenziometro; // serve conversione?
    double R,G,B;
    if (w >= 380 && w < 440) {
        R = -(w - 440.) / (440. - 380.);
        G = 0.0;
        B = 1.0;
    } else if (w >= 440 and w < 490) {
        R = 0.0;
        G = (w - 440.) / (490. - 440.);
        B = 1.0;
    } else if (w >= 490 and w < 510) {
        R = 0.0;
        G = 1.0;
        B = -(w - 510.) / (510. - 490.);
    } else if(w >= 510 and w < 580) {
        R = (w - 510.) / (580. - 510.);
        G = 1.0;
        B = 0.0;
    } else if (w >= 580 and w < 645) {
        R = 1.0;
        G = -(w - 645.) / (645. - 580.);
        B = 0.0;
    } else if (w >= 645 and w <= 780) {
        R = 1.0;
        G = 0.0;
        B = 0.0;
    } else {
        R = 0.0;
        G = 0.0;
        B = 0.0;
    }
#ifdef DEBUGRGB
    Serial.print(R);
    Serial.print(",");
    Serial.print(G);
    Serial.print(",");
    Serial.println(B);
#endif

    vR=MAXVAL*R;
    vG=MAXVAL*G;
    vB=MAXVAL*B;
}
