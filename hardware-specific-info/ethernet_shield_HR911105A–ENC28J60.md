# HR911105A – ENC28J60 Ethernet Shield info

Informazioni per l'ethernet module HR911105A – ENC28J60
![](http://www.yeejeer.com/image/cache/catalog/product/Board%20for%20Arduino%203/ENC28J60%20LAN%20Ethernet%20Network%20Board%20Module--01-500x500.jpg)

Lo shield della HanRun HR911105A è un modulo ethernet basato sul controller ENC28J60 della Microchip. Non è quello ufficiale che si basa su  Wiznet W5100 quindi non è direttamente utilizzabile con la libreria ufficile di Arduino ma servono librerie apposta.
Utilizza il bus SPI per la comunizazione quindi bisogna usare 4 pin per i dati + 2 per l'alimnetazione

## DIFFERENZE tra W5100 e ENC28J60

Entrambi sono dei controller Ethernet, il primo implementa lo stack TCP/IP direttamente in hardware mentre il secondo no. Nel caso dell’ENC28J60 lo stack TCP/IP deve essere implementato via software con un consumo di memoria disponibile non poco trascurabile.

## Librerie
Cercando online mi sono inbattuto in 2 librerie (le più decenti, sebra):
- [EtherCard](https://github.com/njh/EtherCard)
- [UIPEthernet](https://github.com/UIPEthernet/UIPEthernet)

EtherCard è un driver IPv4 compatibile con le board Arduino e clone basate su AVR (quindi non dovrebbe funzionare su ESP32 anche se provando complila e funziona quindi non so, da verificare meglio). Utilizza delle [API](https://www.aelius.com/njh/ethercard/) proprietarie incompatibili con quelle ufficiali, quindi bisogna scrivere il codice in funzione di esse.

UIPEthernet invece è una libreia per ENC28J60 che è compatibile con le API ufficiali di Arduino e del chipset W5100 quindi è possibile usare tutti gli sketch già scritti per Ethernet.h semplicemente importando UIPEthernet al posto di essa. Problema maggiore è che occupa un sacco di spazio (su ESP32 mi sava il 56% di mem occupata senza altre librerie importate).

## Wiring su ESP32

Dato che lo shield usa SPI io l'ho connesso così:
![](https://i.imgur.com/6EiwWq0.png)
